package shortlink.limiter;

public interface Limiter {
    boolean getPermit();
}
