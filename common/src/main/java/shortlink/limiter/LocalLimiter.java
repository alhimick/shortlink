package shortlink.limiter;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class LocalLimiter implements Limiter {
    private final AtomicInteger permits;

    public LocalLimiter(int linksLimit, long renewInterval) {
        permits = new AtomicInteger(linksLimit);
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(() -> permits.set(linksLimit), renewInterval, renewInterval,
                TimeUnit.MILLISECONDS);
    }

    @Override
    public boolean getPermit() {
        return permits.decrementAndGet() >= 0;
    }
}
