package shortlink.limiter;



import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LocalLimiterTest {

    @Test
    void getPermit() {
        Limiter localLimiter = new LocalLimiter(10, 10000);
        assertTrue(localLimiter.getPermit());
    }

    @Test
    void permitsLimit() {
        Limiter localLimiter = new LocalLimiter(1, 10000);
        assertTrue(localLimiter.getPermit());
        assertFalse(localLimiter.getPermit());
    }

    @Test
    void permitsRenew() throws InterruptedException {
        Limiter localLimiter = new LocalLimiter(1, 1000);
        assertTrue(localLimiter.getPermit());
        Thread.sleep(2000);
        assertTrue(localLimiter.getPermit());
    }
}