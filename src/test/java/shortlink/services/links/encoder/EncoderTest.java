package shortlink.services.links.encoder;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EncoderTest {

    @Test
    void testDecode() {
        assertEquals(1000L, Encoder.decode("iq"));
    }

    @Test
    void testEncode() {
        assertEquals("iq", Encoder.encode(1000L));
    }

    @Test
    void testZero() {
        assertEquals("", Encoder.encode(0));
        assertEquals(1, Encoder.decode(Encoder.encode(1)));
    }

    @Test
    void testMaxValue() {
        assertEquals(Long.MAX_VALUE, Encoder.decode(Encoder.encode(Long.MAX_VALUE)));
        assertEquals(Long.MIN_VALUE + 1, Encoder.decode(Encoder.encode(Long.MIN_VALUE + 1)));
    }

    @ParameterizedTest
    @ValueSource(chars = {'+', '-', '?', '!', '@', '#'})
    void testIncorrect(char symbol) {
        assertThrows(RuntimeException.class, () -> Encoder.decode(new String(new char[]{symbol})));
    }
}