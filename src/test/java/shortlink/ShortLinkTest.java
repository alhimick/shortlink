package shortlink;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import shortlink.controller.LinkTO;
import shortlink.controller.ShortLinkController;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {ShortLinkApi.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ShortLinkTest {
    @Autowired
    private ShortLinkController controller;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void contexLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    void linkGenerationIdempotent() {
        String link = "https://bash.im";
        LinkTO shortLink = callLinkEndpoint(link, "shorten");
        LinkTO anotherShortLink = callLinkEndpoint(link, "shorten");
        assertThat(shortLink.getLink()).isEqualTo(anotherShortLink.getLink());
    }

    @Test
    void linkGenerationAndResolve() {
        String link = "https://bash.im";
        LinkTO shortLink = callLinkEndpoint(link, "shorten");
        LinkTO fullLink = callLinkEndpoint(shortLink.getLink(), "resolve");
        assertThat(fullLink.getLink()).isEqualTo(link);
    }

    @Test
    void testRedirect() {
        String link = "https://bash.im";
        LinkTO shortLink = callLinkEndpoint(link, "shorten");
        ResponseEntity<?> response = restTemplate.getForEntity(shortLink.getLink(), Object.class);
        assertThat(response.getHeaders().containsKey("Location")).isTrue();
        assertThat(response.getHeaders().get("Location")).contains(link);
    }

    private LinkTO callLinkEndpoint(String link, String endpoint) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LinkTO> linkRequestEntity = new HttpEntity<>(new LinkTO(link), headers);
        return restTemplate.postForObject("http://localhost:" + port + "/" + endpoint, linkRequestEntity, LinkTO.class);
    }
}
