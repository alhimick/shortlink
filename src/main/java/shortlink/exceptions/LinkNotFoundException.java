package shortlink.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such full link")
public class LinkNotFoundException extends RuntimeException {
    public LinkNotFoundException(Throwable cause) {
        super("No full link stored for this short link", cause);
    }
}
