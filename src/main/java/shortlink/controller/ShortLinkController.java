package shortlink.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import shortlink.services.links.ShortLinkService;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class ShortLinkController {
    private static final String REDIRECT_ENDPOINT = "/short/";

    private final ShortLinkService service;

    @Autowired
    public ShortLinkController(ShortLinkService service) {
        this.service = service;
    }

    @PostMapping(value = "/resolve", produces = MediaType.APPLICATION_JSON_VALUE)
    public LinkTO resolveFullLink(@RequestBody LinkTO body) {
        String shortLink = body.getLink();
        UriComponentsBuilder.fromHttpUrl(body.getLink()); // validation
        List<String> path = UriComponentsBuilder.fromHttpUrl(shortLink).build().getPathSegments();
        return new LinkTO(service.resolveLink(path.get(path.size() - 1)));
    }

    @PostMapping(value = "/shorten", produces = MediaType.APPLICATION_JSON_VALUE)
    public LinkTO getShortLink(@RequestBody LinkTO body, HttpServletRequest request) {
        UriComponentsBuilder.fromHttpUrl(body.getLink()); // validation
        UriComponents uri = UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString()).build();
        String base = uri.getScheme() + "://" + uri.getHost();
        if (uri.getPort() != -1)
            base += ":" + uri.getPort();
        base += REDIRECT_ENDPOINT;
        return new LinkTO(service.getShortLink(body.getLink(), base));
    }

    @GetMapping(value = REDIRECT_ENDPOINT + "{key}")
    public ResponseEntity<Void> redirect(@PathVariable String key) {
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(service.resolveLink(key)).build();
        return ResponseEntity.status(302).location(uriComponents.toUri()).build();
    }

    @RequestMapping("/")
    public ResponseEntity<Void> home() throws URISyntaxException {
        return ResponseEntity.status(302).location(new URI("swagger-ui.html")).build();
    }
}
