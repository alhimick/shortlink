package shortlink.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LinkTO {
    @JsonProperty("link")
    private String link;

    public LinkTO(String link) {
        this.link = link;
    }

    public LinkTO() {
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "LinkTO{" +
                "link='" + link + '\'' +
                '}';
    }
}
