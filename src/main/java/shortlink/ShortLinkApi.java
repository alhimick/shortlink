package shortlink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ShortLinkApi {

    public static void main(String[] args) {
        SpringApplication.run(ShortLinkApi.class, args);
    }
}