package shortlink.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import shortlink.limiter.Limiter;
import shortlink.limiter.LocalLimiter;
import shortlink.services.links.limiter.RemoteLimiter;

@Configuration
public class LimiterFactory {
    private final Config config;

    @Autowired
    public LimiterFactory(Config config) {
        this.config = config;
    }

    @Bean
    public Limiter getLimiter() {
        String address = config.getLimiterAddress();
        return address == null || address.isEmpty() ?
                new LocalLimiter(config.getLinksLimit(), config.getLinksLimitRenew()) :
                new RemoteLimiter(config);
    }
}
