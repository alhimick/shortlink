package shortlink.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class Config {
    @Value("${link.ttl}")
    private long linkTTL;

    @Value("${links.limit:#{null}}")
    private Integer linksLimit;

    @Value("${links.limit.renew:#{null}}")
    private Long linksLimitRenew;

    @Value("${synchronizer.address:#{null}}")
    private String limiterAddress;

    public long getLinkTTL() {
        return linkTTL;
    }

    public int getLinksLimit() {
        return linksLimit;
    }

    public long getLinksLimitRenew() {
        return linksLimitRenew;
    }

    public String getLimiterAddress() {
        return limiterAddress;
    }
}
