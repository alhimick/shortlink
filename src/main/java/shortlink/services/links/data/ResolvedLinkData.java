package shortlink.services.links.data;


public class ResolvedLinkData {
    private final String link;
    private final long expirationTime;

    public ResolvedLinkData(String link, long expirationTime) {
        this.link = link;
        this.expirationTime = expirationTime;
    }

    public String getLink() {
        return link;
    }

    public long getExpirationTime() {
        return expirationTime;
    }
}
