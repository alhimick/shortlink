package shortlink.services.links.limiter;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.client.RestTemplate;
import shortlink.configuration.Config;
import shortlink.exceptions.PermitNotGrantedException;
import shortlink.limiter.Limiter;

public class RemoteLimiter implements Limiter {
    private final RestTemplate template = new RestTemplate();
    private final String address;

    public RemoteLimiter(Config config) {
        address = config.getLimiterAddress();
    }

    @Override
    public boolean getPermit() {
        try {
            return template.getForEntity(address, LimiterResponse.class).getBody().permit;
        } catch (Exception ignore) {
            throw new PermitNotGrantedException();
        }
    }

    static class LimiterResponse {
        private boolean permit;

        public LimiterResponse() {
        }

        @JsonProperty("permit")
        public void setPermit(boolean permit) {
            this.permit = permit;
        }
    }
}
