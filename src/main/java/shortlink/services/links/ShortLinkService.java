package shortlink.services.links;

public interface ShortLinkService {
    String getShortLink(String sourceLink, String host);

    String resolveLink(String key);
}
