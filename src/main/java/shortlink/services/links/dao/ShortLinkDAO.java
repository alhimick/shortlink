package shortlink.services.links.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import shortlink.services.links.data.ResolvedLinkData;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Component
public class ShortLinkDAO {
    private final JdbcTemplate template;

    @Autowired
    public ShortLinkDAO(DataSource ds) {
        template = new JdbcTemplate(ds);
    }

    public Long getExistingId(String sourceLink) {
        try {
            return template.queryForObject("select id from links where source_link = ?", Long.class, sourceLink);
        } catch (EmptyResultDataAccessException ignore) {
            return null;
        }

    }

    public long storeSourceLink(String sourceLink, long expiration) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(template)
                .withTableName("links").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("source_link", sourceLink);
        parameters.put("expiration_time", new Timestamp(expiration));
        return insert.executeAndReturnKey(new MapSqlParameterSource(parameters)).longValue();
    }

    public ResolvedLinkData getSourceLinkData(long id) {
        return template.queryForObject("select source_link, expiration_time from links where id = ?",
                (rs, rowNum) -> new ResolvedLinkData(rs.getString(1), rs.getTimestamp(2).getTime()), id);
    }

    public void updateExpiration(long id, long time) {
        template.update("update links set expiration_time = ? where id = ?", id, time);
    }
}
