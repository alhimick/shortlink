package shortlink.services.links;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import shortlink.configuration.Config;
import shortlink.exceptions.LinkNotFoundException;
import shortlink.limiter.Limiter;
import shortlink.services.links.dao.ShortLinkDAO;
import shortlink.services.links.data.ResolvedLinkData;
import shortlink.services.links.encoder.Encoder;

@Component
public class ShortLinkServiceImpl implements ShortLinkService {
    private final ShortLinkDAO dao;
    private final Config config;
    private final Limiter limiter;

    @Autowired
    public ShortLinkServiceImpl(ShortLinkDAO dao, Config config, Limiter limiter) {
        this.dao = dao;
        this.config = config;
        this.limiter = limiter;
    }

    @Override
    public String getShortLink(String sourceLink, String host) {
        if (!limiter.getPermit())
            throw new IllegalStateException("Permissions exceeded, try again later");
        Long id = dao.getExistingId(sourceLink);
        long expiration = System.currentTimeMillis() + config.getLinkTTL();
        if (id == null) {
            id = dao.storeSourceLink(sourceLink, expiration);
        } else {
            dao.updateExpiration(id, expiration);
        }
        return (host.endsWith("/") ? host : host + "/") + Encoder.encode(id);
    }

    @Override
    public String resolveLink(String key) {
        long id = Encoder.decode(key);
        ResolvedLinkData linkData;
        try {
            linkData = dao.getSourceLinkData(id);
        } catch (EmptyResultDataAccessException e) {
            throw new LinkNotFoundException(e);
        }
        if (linkData.getExpirationTime() <= System.currentTimeMillis())
            throw new IllegalStateException("Link already expired");
        return linkData.getLink();
    }
}
