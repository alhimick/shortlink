package shortlink.services.links.encoder;

import shortlink.exceptions.IncorrectShortKeyException;

public class Encoder {
    private static final char[] ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            .toCharArray();
    private static final char NEGATIVE_MARKER = '=';
    private static final int BASE = ALPHABET.length;
    private static final int ENG_ALPHABET_SIZE = 26;


    private Encoder() {
    }

    public static String encode(long id) {
        StringBuilder result = new StringBuilder();
        long remain = id;
        while (Math.abs(remain) >= 1) {
            int remainder = (int) (remain % BASE);
            result.append(ALPHABET[remainder >= 0 ? remainder : BASE + remainder]);
            remain = remain / BASE;
        }
        return result.toString() + (id >= 0 ? "" : NEGATIVE_MARKER);
    }

    public static long decode(String shortKey) {
        int sign = 1;
        if (shortKey.length() > 0 && shortKey.charAt(shortKey.length() - 1) == NEGATIVE_MARKER) {
            sign = -1;
            shortKey = shortKey.substring(0, shortKey.length() - 1);
        }
        long result = 0;
        for (int i = 0; i < shortKey.length(); i++) {
            char current = shortKey.charAt(i);
            if (current >= 'a' && current <= 'z') {
                result += (current - 'a') * Math.pow(BASE, i);
            } else if (current >= 'A' && current <= 'Z') {
                result += (current - 'A' + ENG_ALPHABET_SIZE) * Math.pow(BASE, i);
            } else if (current >= '0' && current <= '9') {
                result += (current - '0' + ENG_ALPHABET_SIZE * 2) * Math.pow(BASE, i);
            } else {
                throw new IncorrectShortKeyException("Unexpected symbol at " + i + ": " + current);
            }
        }
        return result * sign;
    }
}
