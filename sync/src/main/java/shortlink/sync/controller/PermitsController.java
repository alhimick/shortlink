package shortlink.sync.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shortlink.limiter.Limiter;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class PermitsController {

    private final Limiter limiter;

    @Autowired
    public PermitsController(Limiter limiter) {
        this.limiter = limiter;
    }

    @GetMapping(value = "/permit", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseTO getPermit() {
        return new ResponseTO(limiter.getPermit());
    }

    @RequestMapping("/")
    public ResponseEntity<Void> home() throws URISyntaxException {
        return ResponseEntity.status(302).location(new URI("swagger-ui.html")).build();
    }
}
