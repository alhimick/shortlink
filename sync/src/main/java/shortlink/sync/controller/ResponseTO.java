package shortlink.sync.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseTO {
    @JsonProperty("permit")
    private boolean permit;

    ResponseTO(boolean permit) {
        this.permit = permit;
    }

    public ResponseTO() {
    }

    @JsonProperty("permit")
    public boolean isPermit() {
        return permit;
    }

    @JsonProperty("permit")
    public void setPermit(boolean permit) {
        this.permit = permit;
    }
}
