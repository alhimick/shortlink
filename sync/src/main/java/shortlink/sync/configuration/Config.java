package shortlink.sync.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class Config {
    @Value("${links.limit:#{null}}")
    private Integer linksLimit;

    @Value("${links.limit.renew:#{null}}")
    private Long linksLimitRenew;

    public Integer getLinksLimit() {
        return linksLimit;
    }

    public Long getLinksLimitRenew() {
        return linksLimitRenew;
    }
}
