package shortlink.sync;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import shortlink.limiter.Limiter;
import shortlink.limiter.LocalLimiter;
import shortlink.sync.configuration.Config;


@SpringBootApplication
public class SyncApi {

    public static void main(String[] args) {
        SpringApplication.run(SyncApi.class, args);
    }

    @Autowired
    @Bean
    public Limiter getLimiter(Config config) {
        return new LocalLimiter(config.getLinksLimit(), config.getLinksLimitRenew());
    }
}